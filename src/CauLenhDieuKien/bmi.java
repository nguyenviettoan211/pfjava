package CauLenhDieuKien;
import java.util.Scanner;
public class bmi {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double weight, height, bmi;
        System.out.println("Nhap vao can nang:");
        weight = scanner.nextDouble();
        System.out.println("nhap vao chieu cao:");
        height = scanner.nextDouble();
        bmi = weight/Math.pow(height,2);
        System.out.printf("%-20s%s","bmi","Interpretation\n");
        if (bmi<18)
            System.out.printf("%-20.2s%s",bmi,"underweight");
        else if(bmi<25)
            System.out.printf("%-20.2s%s",bmi,"normal");
        else if(bmi<30)
            System.out.printf("%-20.2s%s",bmi,"overweight");
        else
            System.out.printf("%-20.2f%s",bmi,"obese");
    }
}
