package CauLenhDieuKien;
import java.util.Scanner;
public class switchcase {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter your grade: ");
        String userInput = input.next();
        char grade = userInput.charAt(0);

        switch (grade) {
            case 'a':
                System.out.println("Excellent!");
                break;
            case 'b':
                System.out.println("Great!");
                break;
            case 'c':
            case 'd':
                System.out.println("Well done!");
                break;
            case 'f':
                System.out.println("Sorry, you failed.");
                break;
            default:
                System.out.println("Error! Invalid grade.");
        }
    }
}
