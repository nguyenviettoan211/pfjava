package mang;
import java.util.Scanner;
public class CheckTime {
    public static void main(String[] args) {
        int startTime,endTime,certainTime;
        boolean check;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Program to check whether a certain time (0 - 23) is included within a specific time range");
        do {
            System.out.println("Input certain time:");
            certainTime = scanner.nextInt();
        } while (certainTime < 0 || certainTime > 23);
        do {
            System.out.println("Input start time:");
            startTime = scanner.nextInt();
        } while (startTime < 0 || startTime > 23);
        do {
            System.out.println("Input end time:");
            endTime = scanner.nextInt();
        } while (endTime < 0 || endTime > 23);
        if (startTime < endTime) {
            check = (certainTime >= startTime) && (certainTime < endTime);
        } else if (startTime > endTime) {
            check = (certainTime >= startTime) || (certainTime < endTime);
        } else {
            check = certainTime == startTime;
        }
        if (check) {
            System.out.println("In time range!");
        } else {
            System.out.println("Out of time range!");
        }

    }
}
