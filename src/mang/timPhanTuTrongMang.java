package mang;
import java.util.Scanner;
public class timPhanTuTrongMang {
    public static void main(String[] args) {
        String[] students = {"Christian", "Michael", "Camila", "Sienna", "Tanya", "Connor", "Zachariah", "Mallory", "Zoe", "Emily"};
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhap ten ban can tim");
        String name = scanner.nextLine();
        boolean isExit = false;
        for(int i=0;i<students.length;i++){
          if(students[i].equals(name)){
              System.out.println("So thu tu cua "+name+" la:"+(i+1));
              isExit = true;
              break;
          }
        };
        if (!isExit)
            System.out.println("Khong tim thay '"+name+"' trong danh sach!");
    }
}
